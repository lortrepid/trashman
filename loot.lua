sellList = {}
enabled = false
moneyMade = 0
merchantOpen = false
usableInstanceID = 0

local frame = CreateFrame("FRAME", "TrashmanFrame")
frame:RegisterEvent("CHAT_MSG_LOOT")
frame:RegisterEvent("MERCHANT_SHOW")
frame:RegisterEvent("MERCHANT_CLOSED")
frame:RegisterEvent("MERCHANT_UPDATE")
frame:RegisterEvent("ZONE_CHANGED_NEW_AREA")

local function determineIfAgInstance()
  for tierIndex = 1, EJ_GetNumTiers() do
    EJ_SelectTier(tierIndex)

    instanceName, _, _, _, _, _, _, _, groupSize = GetInstanceInfo()
    local instanceID, name, description, _, _, _, _, link = EJ_GetInstanceByIndex(1, false)
    localIndex = 1

    while instanceID do
      if (instanceID == EJ_GetCurrentInstance() or instanceName == name) then
        print("instance is in list")
        usableInstanceID = instanceID
        enabled = true
        return;
      end
      instanceID, name, description, _, _, _, _, link = EJ_GetInstanceByIndex(localIndex, false)
      localIndex = localIndex + 1
    end

    local instanceID, name, description, _, _, _, _, link = EJ_GetInstanceByIndex(1, true)
    localIndex = 1

    while instanceID do
      if (instanceID == EJ_GetCurrentInstance() or instanceName == name) then
        print("instance is in list")
        usableInstanceID = instanceID
        enabled = true
        return;
      end
      instanceID, name, description, _, _, _, _, link = EJ_GetInstanceByIndex(localIndex, true);
      localIndex = localIndex + 1
    end
  end
  enabled = false
end

local function eventHandler(self, event, ...)
  if (event == "CHAT_MSG_LOOT") then

    local msg, user = ...
    for itemLink in msg:gmatch("|%x+|Hitem:.-|h.-|h|r") do
      if (enabled) then
        EJ_SelectInstance(usableInstanceID)
        _, _, tempDiff, _, _, _, _, _, _ = GetInstanceInfo()
        EJ_SetDifficulty(tempDiff)
        if (isItemInInstance(itemLink)) then
          table.insert(sellList, itemLink)
        end
      end
    end
  end

  if (event == "ZONE_CHANGED_NEW_AREA") then
    C_Timer.After(.5, determineIfAgInstance)
  end

  if (event == "MERCHANT_UPDATE") then
    count = 0
    if (merchantOpen) then
      for bag=0,4 do
        for slot=1, GetContainerNumSlots(bag) do
          local link = GetContainerItemLink(bag, slot)
          if link then
            itemInTable = isItemInSlotInTable(bag, slot)
            _, _, quality, _, _, _, _, _, _, _, cost = GetItemInfo(itemID)
            if (quality == 0) then
              moneyMade = moneyMade + cost
              UseContainerItem(bag, slot)
              count = count + 1
            elseif itemInTable then
              if (quality ~= 5) then
                moneyMade = moneyMade + cost
                UseContainerItem(bag, slot)
                count = count + 1
              end
            end
          end
          if (count == 12) then
            return
          end
        end
      end
    end
  end

  if (event == "MERCHANT_CLOSED") then
    merchantOpen = false
    sellList = {}
    print ("Total Money Made From Selling: ", moneyMade/10000, "g")
    moneyMade = 0
    usableInstanceID = 0
  end

  if (event == "MERCHANT_SHOW") then
    merchantOpen = true
    for bag=0,4 do
      for slot=1, GetContainerNumSlots(bag) do
        local link = GetContainerItemLink(bag, slot)
        if link then
          itemInTable = isItemInSlotInTable(bag, slot)
          _, _, quality, _, _, _, _, _, _, _, cost = GetItemInfo(itemID)
          if (quality == 0) then
            moneyMade = moneyMade + cost
            UseContainerItem(bag, slot)
            return
          elseif itemInTable then
            if (quality ~= 5) then
              moneyMade = moneyMade + cost
              UseContainerItem(bag, slot)
              return
            end
          end
        end
      end
    end
  end
end

frame:SetScript("OnEvent", eventHandler)

function wait(seconds)
  local _start = os.time()
  local _end = _start+seconds
  while (_end ~= os.time()) do
  end
end

function isItemInInstance(itemLink)
  for index=1, EJ_GetNumLoot() do
    dbID, encID, itemName, _, _, _, dbLink = EJ_GetLootInfoByIndex(index);
    --print("ItemID: ", itemID, "dbID: ", dbID)
    if itemLink == dbLink then
      return true;
    end
  end
  return false;
end

function isItemInSlotInTable(bag, slot)
  texture, count, locked, quality, readable, lootable, link, isFiltered, hasNoValue, itemID = GetContainerItemInfo(bag, slot)

  for index = 1, table.getn(sellList) do
    if (link == sellList[index]) then
      table.remove(sellList, index)
      return true
    end
  end
  return false
end

function getIdFromLink(link)
  local justID = string.gsub(link,".-\124H([^\124]*)\124h.*", "%1")
  local itype, itemid, enchantId, gem1, gem2, gem3, gem4, suffixID, uniqueID, level, upgradeId, instanceDifficultyID, numBonusIDs, bonusID1, bonusID2 = strsplit(":",justID)
  return tonumber(itemid)
end
