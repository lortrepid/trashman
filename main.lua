local frame = CreateFrame("FRAME", "TrashmanFrame")
frame:RegisterEvent("MERCHANT_SHOW")
local function eventHandler(self, event, ...)
  local moneyMade = 0;
  for bag=0,4 do
    for slot=1, GetContainerNumSlots(bag) do
      local link = GetContainerItemLink(bag, slot)
      if link then
        isItemInSlotInTable(bag, slot)
        _, _, quality, _, _, _, _, _, _, _, cost = GetItemInfo(itemID)
        if (quality == 0) then
          moneyMade = moneyMade + cost
          UseContainerItem(bag, slot)
        end

        if isItemInSlotInTable(bag, slot) then
          print ("Selling ItemID: ", itemID)
          if (quality ~= 5) then
            moneyMade = moneyMade + cost
            UseContainerItem(bag, slot)
          end
        end
      end
    end
  end
  print ("Total Money Made From Selling: ", moneyMade/10000, "g")
end
frame:SetScript("OnEvent", eventHandler)


function isItemInSlotInTable(bag, slot)
  texture, count, locked, quality, readable, lootable, link, isFiltered, hasNoValue, itemID = GetContainerItemInfo(bag, slot)

  if (isItemFromAQTen(itemID)) then
    return true
  elseif (isItemFromFirelands(itemID)) then
    return true
  elseif (isItemFromJadeTemple(itemID)) then
    return true
  elseif (isItemFromMC(itemID)) then
    return true
  elseif (isItemFromBWL(itemID)) then
    return true
  else
    return false
  end
end

function isItemFromAQTen(itemID)
  EJ_SelectTier(1)
  EJ_SelectInstance(743)
  return isItemInInstance(itemID)
end

function isItemFromJadeTemple(itemID)
  EJ_SelectTier(4)
  EJ_SelectInstance(313)
  EJ_SetDifficulty(2)
  return isItemInInstance(itemID)
end

function isItemFromMC(itemID)
  if itemID == 93034 then
    return false
  elseif itemID == 93035 then
    return false
  elseif itemID == 17203 then
    return false
  elseif itemID == 138833 then
    return false
  else
    EJ_SelectTier(1)
    EJ_SelectInstance(741)
    return isItemInInstance(itemID)
  end
end

function isItemFromBWL(itemID)
  if itemID == 93037 then --Blackwing Banner
    return false
  elseif itemID == 93036 then --Unscathed Egg
    return false
  elseif itemID == 93038 then --Whistle of Chromatic Bone
    return false
  else
    EJ_SelectTier(1)
    EJ_SelectInstance(742)
    return isItemInInstance(itemID)
  end
end

function isItemFromFirelands(itemID)
  EJ_SelectTier(4)
  EJ_SelectInstance(73)
  EJ_SetDifficulty(3)

  if (isItemInInstance(itemID)) then
    return true;
  else
    EJ_SetDifficulty(4)
  end

  if (isItemInInstance(itemID)) then
    return true;
  else
    EJ_SetDifficulty(5)
  end

  if (isItemInInstance(itemID)) then
    return true;
  else
    EJ_SetDifficulty(6)
  end

  if (isItemInInstance(itemID)) then
    return true;
  else
    return false;
  end
end

function isItemInInstance(itemID)
  for index=1, EJ_GetNumLoot() do
    dbID, encID, itemName, _, _, _, itemLink = EJ_GetLootInfoByIndex(index);
    --print("ItemID: ", itemID, "dbID: ", dbID)
    if itemID == dbID then
      print("Item from Instance found", link)
      return true;
    end
  end
  return false;
end
